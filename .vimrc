"Configure Pathogen
filetype off
execute pathogen#infect()

"General Settings
syntax on
filetype plugin indent on
colorscheme desert
hi CursorLine term=bold cterm=bold guibg=Grey40

"Text Settings
set fu
set guifont=Monaco:h18
set cursorline
set autoindent
set list
set listchars=tab:>-,trail:.,extends:>,precedes:<,nbsp:_
set virtualedit=all
set number
set backspace=indent,eol,start
set ic
set nowrap

"Key Re-Mappings
map <space> /
imap jj <C-C>
map <C-j> <C-W>j
map <C-k> <C-W>k
map <C-h> <C-W>h
map <C-l> <C-W>l
map <C-=> <C-W>=

"Disable Arrow Keys and Esc for learning
inoremap  <Up>     <NOP>
inoremap  <Down>   <NOP>
inoremap  <Left>   <NOP>
inoremap  <Right>  <NOP>
inoremap  <Esc>    <NOP>
noremap <Up> <NOP>
noremap <Down> <NOP>
noremap <Left> <NOP>
noremap <Right> <NOP>

"Plugin Settings
map <leader>nn :NERDTreeToggle<cr>
map <leader>nb :NERDTreeFromBookmark
map <leader>nf :NERDTreeFind<cr>

map <leader>o :BufExplorer<cr>
